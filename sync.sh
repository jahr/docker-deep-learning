#!bash
set -e

# keep a remote folder in sync with the local folder

CODE_DIR_LOCAL=code/
CODE_DIR_REMOTE=/data/code/
MACHINE_NAME=docker-deep-learning

SSH_PORT=$(docker-machine inspect $MACHINE_NAME --format={{.Driver.SSHPort}})
SSH_USER=$(docker-machine inspect $MACHINE_NAME --format={{.Driver.SSHUser}})
SSH_KEY=$(docker-machine inspect $MACHINE_NAME --format={{.Driver.SSHKeyPath}})
DOCKER_IP=$(docker-machine ip $MACHINE_NAME)

while true ; do
  inotifywait -r -q -e modify,attrib,close_write,move,create,delete $CODE_DIR_LOCAL
  rsync -avrzh --progress \
  --exclude='__pycache__' \
  -e "ssh -i $SSH_KEY" $CODE_DIR_LOCAL $SSH_USER@$DOCKER_IP:$CODE_DIR_REMOTE
done

# Part 1: How to set up docker-machine (on local machine)

## Install `docker-machine`

```
# Download and install docker-machine
curl -L https://github.com/docker/machine/releases/download/v0.15.0/docker-machine-$(uname -s)-$(uname -m) > bla
chmod +x bla
sudo mv bla /usr/local/bin/docker-machine

# Opt out of error-reporting
mkdir -p ~/.docker/machine && touch ~/.docker/machine/no-error-report
```

References:
- https://github.com/docker/machine/releases/tag/v0.15.0


## Basic usage of `docker-machine`

Spin up some instance on AWS
```
docker-machine create --driver amazonec2 --amazonec2-region eu-west-1 dockermachinetest
```

See currently running machines
```
docker-machine ls
```

Mark the new machine as the active machine, so that `docker` will use the remote host's docker engine to run containers
```
eval $(docker-machine env dockermachinetest)
```

Run a container on the remote host
```
docker run -it ubuntu free -h
```

Kill any remote container
```
docker rm `docker ps -aq`
```

Revert that `docker` uses local docker engine
```
eval $(docker-machine env -u)
```

Terminate the instance
```
docker-machine
```

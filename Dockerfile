# Docker image for deep learning
#
# This docker images provides:
# - Ubuntu 16.04
# - CUDA 9.0
# - CUDA toolkit libraries
# - cuDNN 7
# - Virtualenv (Python 3.6) with pre-installed PyPI packages, incl.
#   - Keras / Tensorflow (GPU) / Pytorch
#   - Jupyter notebook
#   - Numpy, Scipy, Pandas,
#   - Sklearn, Matplotlib
#
# Tested with this host configuration:
# - Ubuntu 18.04
# - Nvidia driver 396.54
# - Docker 18.06.1-ce
# - Nvidia Docker 2.0.3

FROM nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04

RUN apt-get update && apt-get install -y --no-install-recommends \
        ca-certificates \
        locales \
        build-essential \
        locate \
        software-properties-common \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

ENV DEBIAN_FRONTEND noninteractive
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
locale-gen

# Install Python 3.6, with pip and virtualenv
RUN apt-get update \
    && add-apt-repository ppa:jonathonf/python-3.6 \
    && apt-get update \
    && apt-get install -y python3.6 python3-pip python3.6-dev \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
RUN ln -s /usr/bin/python3 /usr/local/bin/python
RUN pip3 install -U pip virtualenv setuptools

# Command line tools
RUN apt-get update && apt-get install -y --no-install-recommends \
        vim \
        git \
        ssh \
        curl \
        wget \
        tmux \
        htop \
        sudo \
        locate \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y --no-install-recommends \
        awscli \
        python-opencv \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Add user (with passwordless sudo permissions) and continue as user
ENV USERNAME user
RUN useradd -ms /bin/bash $USERNAME
RUN adduser $USERNAME sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER $USERNAME
ENV PATH $PATH:/home/$USERNAME/.local/bin
WORKDIR /home/$USERNAME/

# Create virtualenv with commonly used packes
RUN virtualenv -p python3.6 ~/.virtualenv/python3.6
SHELL ["/bin/bash", "-c"]

# Install commonly used packages into the virtualenv
RUN source ~/.virtualenv/python3.6/bin/activate \
    && pip install \
        numpy==1.14.5 \
        keras==2.2.2 \
        tensorflow-gpu==1.10 \
        torch \
        torchvision \
        torchtext \
        scipy \
        jupyter \
        notebook \
        matplotlib \
        pandas \
        sklearn \
        tqdm \
    && rm -rf ~/.cache

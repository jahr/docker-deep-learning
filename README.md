# Containerized Deep Learning on GPU using AWS spot instances

Setting up a GPU enabled deep-learning environment can be a litte pain when facing issues like incompatible versions of OS, Nvidia drivers, CUDA libraries, and Tensorflow. Using Amazon machine images (AMI) and docker, the setup becomes quick and easy to reproduce.

The Amazon machine image (AMI) provides:

- Ubuntu 18.04
- Nvidia driver 396.54
- Docker 18.06.1-ce
- Nvidia Docker 2.0.3

The docker image provides:

- Ubuntu 16.04
- CUDA 9.0
- CUDA toolkit libraries
- cuDNN 7
- Python 3.6
- a python virtualenv with pre-installed PyPI packages:
  - Keras, Tensorflow (GPU), Pytorch (incl. torchvision and torchtext)
  - Jupyter notebook
  - Numpy, Scipy, Pandas
  - Sklearn, Matplotlib

This file provides just the necessary steps to get it running, but for a more bottom-up setup:

1. How to set up docker machine: 1_docker-machine.md
2. Containerized deep learning with Nvidia-Docker (on local machine): 2_nvidia-docker_local.md
3. Spin up a remote docker host using an AWS spot instance: 3_nvidia-docker_aws.md

## Prerequisites:

- installed `docker-machine` (see 1_docker-machine.md)
- AWS CLI with a working configuration (`aws configure`)

## Spin up a remote docker host using an AWS spot instance

Settings some variables:

```
# Set variables
REGION=eu-west-1
INSTANCE_TYPE=p2.xlarge
VOLUME_SIZE=120
AMI=ami-0d1bd97eab9822698
IAM_INSTANCE_PROFILE=s3-full-access
MAX_PRICE=0.5
MACHINE_NAME=docker-deep-learning

# Create spot instance
docker-machine create --driver amazonec2  \
--amazonec2-region $REGION \
--amazonec2-instance-type $INSTANCE_TYPE \
--amazonec2-root-size $VOLUME_SIZE \
--amazonec2-ami $AMI \
--amazonec2-iam-instance-profile $IAM_INSTANCE_PROFILE \
--amazonec2-request-spot-instance \
--amazonec2-spot-price $MAX_PRICE \
$MACHINE_NAME
```

## Mount volume for persistance

```
# Set some parameters
VOLUME_ID=vol-0e764a6f46f2be692
DEVICE=/dev/xvdg
MOUNT_POINT=/data
INSTANCE_ID=$(docker-machine inspect $MACHINE_NAME | jq '.["Driver"]["InstanceId"]' | tr -d '"')

# Attach a volume to the machine
aws ec2 attach-volume \
  --device $DEVICE \
  --instance-id $INSTANCE_ID \
  --volume-id $VOLUME_ID

# Mount volume to the machine
docker-machine ssh $MACHINE_NAME sudo mkdir -p $MOUNT_POINT
docker-machine ssh $MACHINE_NAME sudo mount $DEVICE $MOUNT_POINT
docker-machine ssh $MACHINE_NAME sudo chown -R ubuntu:ubuntu /data

# Test if volume was mounted
docker-machine ssh $MACHINE_NAME df -h
```

## Using remote containers

Configure the local `docker` command to use the docker-engine on the new remote machine (instead of using the local docker-engine). This is done by the following, which is basically setting some environment variables in you shell.
```
eval $(docker-machine env $MACHINE_NAME)
export NV_HOST="ssh://ubuntu@$(docker-machine ip $MACHINE_NAME):"
ssh-add ~/.docker/machine/machines/$MACHINE_NAME/id_rsa
```

From now on, all `docker` commands will use the remote host, for example:
```
# Build the image and make it available to the remote docker host
docker build . -t jonas/deep-learning

# Start the remote container
docker run --runtime=nvidia -v /data:/data -it jonas/deep-learning bash
```

To use the local docker-engine again, revert the environment variables:
```
eval $(docker-machine env -u)
```

## Port forwarding via SSH

To access port from the remote docker container, for thing like Jupyter notebook or Tensorboard, it's basically hops

1. Share ports of the remote container with the remote docker-engine (e.g. using `docker run -p 9999:9999 stuff`)

2. Forward ports from local machien to remote docker host:

```
MACHINE_NAME=docker-deep-learning
SSH_PORT=$(docker-machine inspect $MACHINE_NAME --format={{.Driver.SSHPort}})
SSH_USER=$(docker-machine inspect $MACHINE_NAME --format={{.Driver.SSHUser}})
SSH_KEY=$(docker-machine inspect $MACHINE_NAME --format={{.Driver.SSHKeyPath}})
DOCKER_IP=$(docker-machine ip $MACHINE_NAME)
ssh -i $SSH_KEY -p $SSH_PORT -L 9999:localhost:9999 $SSH_USER@$DOCKER_IP
```
## Cancel the spot instance request and terminate the instance

```
INSTANCE_ID=$(docker-machine inspect $MACHINE_NAME | jq '.["Driver"]["InstanceId"]' | tr -d '"')

# Get spot instance request id
SPOT_INSTANCE_REQUEST_ID=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID | jq '.["Reservations"][]["Instances"][]["SpotInstanceRequestId"]' | tr -d '"')

# Cancel the spot instance request
aws ec2 cancel-spot-instance-requests --spot-instance-request-ids $SPOT_INSTANCE_REQUEST_ID

# Terminate the instance
aws ec2 terminate-instances --instance-ids $INSTANCE_ID

# Inform docker-machine that the instance doesn't exist anymore
docker-machine rm -y $MACHINE_NAME
```

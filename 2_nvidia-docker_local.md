# Part 2: Containerized deep learning with Nvidia-Docker

Setting up a GPU enabled deep-learning environment can be a litte pain when facing issues like incompatible versions of OS, Nvidia drivers, CUDA libraries, and Tensorflow. Using docker, the setup becomes faster to reproduce.

In the following, I describe my setup on an Ubuntu 18.04 system.

## Setup host system

To create GPU enabled containers, three things are needed on the host:

- nvidia drivers (396.54)
- docker (18.06.1-ce)
- nvidia-docker (2.03)

### Step 1: Install nvidia Drivers

On Ubuntu 18.04, install the proprietary NVIDIA drivers.

```
sudo add-apt-repository ppa:graphics-drivers/ppa -y
sudo apt-get update -y
sudo apt install nvidia-kernel-source-396
sudo apt install nvidia-driver-396
```

### Step 2: Install docker

This gave me Docker version 18.06.1-ce

```
# Install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
sudo apt install docker-ce

# Start docker service (at boot)
sudo systemctl status docker

# Add user to docker group
sudo usermod -aG docker ${USER}
su - ${USER}
```

### Step 3: Install nvidia-docker

Nvidia-docker lets the host system expose it's GPUs to the dockers containers running on it.

```
# Add the package repositories
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update

# Install nvidia-docker2
sudo apt-get install -y nvidia-docker2
```

Optional: to make `--runtime=nvidia` the default for all new containers:
```
sudo su
cat <<"EOF" > /etc/docker/daemon.json
{
    "default-runtime": "nvidia",
    "runtimes": {
        "nvidia": {
            "path": "/usr/bin/nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}
EOF
```

Restart the docker deamon for changes to take effect
```
sudo pkill -SIGHUP dockerd
```

### Step 4: Test the setup (optional)

Now GPU-enabled containers can be created, e.g.
```
docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi
```

References:
- https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)
- https://github.com/NVIDIA/nvidia-docker)

## Example usage

To start the container, I would `cd` to my favourite deep-learning project and run:
```
docker run --volume=`pwd`:/home/user/${PWD##*/} --net=host -ti jonas/deep-learning bash
```

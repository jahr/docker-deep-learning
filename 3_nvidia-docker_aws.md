# Part 3: Creation of an AWS machine image

## Spin up a remote docker host using an AWS spot instance

Settings some constants
```
REGION=eu-west-1
INSTANCE_TYPE=p2.xlarge
VOLUME_SIZE=120
AMI=`bash ./get-ubuntu-ami.sh $REGION ubuntu-bionic-18.04`
SECURITY_GROUP_HOME=Home
MAX_PRICE=0.5
MACHINE_NAME=my-instance
```

Then create the spot instance

```
docker-machine create --driver amazonec2  \
--amazonec2-region $REGION \
--amazonec2-instance-type $INSTANCE_TYPE \
--amazonec2-root-size $VOLUME_SIZE \
--amazonec2-ami $AMI \
--amazonec2-security-group $SECURITY_GROUP_HOME \
--amazonec2-request-spot-instance \
--amazonec2-spot-price $MAX_PRICE \
$MACHINE_NAME
```

## Provisioning the remote docker host

Restart machine (not sure if this is required)

```
docker-machine restart $MACHINE_NAME
```

`ssh` into the new instance
```
docker-machine ssh $MACHINE_NAME
```
Update system packages
```
sudo apt-get update
sudo apt-get upgrade -y
```

Install nvidia-drivers
```
sudo add-apt-repository ppa:graphics-drivers/ppa -y
sudo apt-get update -y
sudo apt-get install -y nvidia-kernel-source-396 nvidia-driver-396
sudo apt-get clean
```

Install nvidia-docker
```
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
sudo apt-get install -y nvidia-docker2
```
Now leave the machine, using Ctrl-D.

Restart the machine again, to finish provisioning:
```
docker-machine restart $MACHINE_NAME
```

## Test if things work

Now this should work on the remote docker host
```
docker-machine ssh $MACHINE_NAME
nvidia-smi
```
Leave the remote machine again , using Ctrl-D.


## Create AMI image

After finishing provisioning, it may be a good idea to create an AMI image for future use, so that the previous provisioning steps can e skipped next time.

```
INSTANCE_ID=$(docker-machine inspect $MACHINE_NAME | jq '.["Driver"]["InstanceId"]' | tr -d '"')
AMI=$(aws ec2 create-image --instance-id $INSTANCE_ID --name ubuntu18.04-nvidia-docker) | jq '.["ImageId"]' | tr -d '"'
echo $AMI
```

Read in part 4, how to use remote containers, and how to terminate them when done.
